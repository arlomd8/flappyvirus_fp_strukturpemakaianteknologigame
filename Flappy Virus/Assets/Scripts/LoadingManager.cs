﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class LoadingManager : MonoBehaviour
{
    Animator _animation;
    public GameObject playButton, loadingText;

    IEnumerator WaitLoadScene() {
        yield return new WaitForSeconds(10);
        playButton.SetActive(true);
        loadingText.GetComponent<TextMeshProUGUI>().text = "LOADING COMPLETE !";
    }
    
    void Update()
    {
        StartCoroutine(WaitLoadScene());
    }

    public void StartGame()
    {
        SceneManager.LoadScene(2);
    }
}
