﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    public static Score instance;
    public int highScore, collectedScore,currentHighScore;
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        if(collectedScore > highScore)
        {
            highScore = collectedScore;
        }

    }
    
    public int CurrentHighScore()
    {
        if(highScore > currentHighScore)
        {
            currentHighScore = highScore;
        }
        return currentHighScore;
    }

    public int NewHighScore()
    {
        return highScore;
    }
}
