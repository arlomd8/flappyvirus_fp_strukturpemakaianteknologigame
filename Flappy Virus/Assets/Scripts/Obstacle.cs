﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public Vector2 velocity = new Vector2(-4, 0);
    public float range = 4;
    void Start()
    {
        if (GameManager.instance.gameStart)
        {
            GetComponent<Rigidbody2D>().velocity = velocity;
            transform.position = new Vector3(transform.position.x, transform.position.y - range * Random.value, transform.position.z);
        }
        
    }

    void Update()
    {
        if (!GameManager.instance.gameStart)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Destroyer"))
        {
            Destroy(this.gameObject);
        }
    }
}
