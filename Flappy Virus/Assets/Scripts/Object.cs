﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class Object : MonoBehaviour
{
    public TextMeshProUGUI highScore, collectedScore, testResult;
    public GameObject newHighScore;
    private int currentHighScore;

    public void Start()
    {
        //if (Score.instance.collectedScore > Score.instance.highScore)
        //{
        //    newHighScore.SetActive(true);
        //}
        //else
        //{
        //    newHighScore.SetActive(false);
        //}
       
        currentHighScore = Score.instance.currentHighScore;
        

        collectedScore.text = "Collected Score : " + Score.instance.collectedScore.ToString();
        highScore.text = "High Score : " + currentHighScore.ToString();

        if (Score.instance.CurrentHighScore() > currentHighScore)
        {
            newHighScore.SetActive(true);
            testResult.text = "Negative";
        }
        else
        {
            newHighScore.SetActive(false);
            testResult.text = "Positive";
        }


    }
    public void ButtonClick()
    {
        if (Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Click");
        }
    }

    public void ButtonHome()
    {
        SceneManager.LoadScene(0);
    }

}
