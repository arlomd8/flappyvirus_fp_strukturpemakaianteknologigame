﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Vector2 jumpForce = new Vector2(0, 100);
    public Generate generate;
    
    void Update()
    {
        if (GameManager.instance.gameStart)
        {
            GetComponent<Rigidbody2D>().isKinematic = false;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                GetComponent<Rigidbody2D>().AddForce(jumpForce);
                if (Menu.sfxOn)
                {
                    FindObjectOfType<AudioManager>().Play("Jump");
                }
            }

            Vector2 screenPostion = Camera.main.WorldToScreenPoint(transform.position);
            if (screenPostion.y > Screen.height || screenPostion.y < 0)
            {
                Die();
            }
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0,0));
            GetComponent<Rigidbody2D>().isKinematic = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Fruit"))
        {
            generate.score *= 2;
            if (Menu.sfxOn)
            {
                FindObjectOfType<AudioManager>().Play("SkillActive");
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Die();
        if (Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Collide");
        }
    }
    void Die()
    {
        GameManager.instance.gameStart = false;
        GameManager.instance.isolasiDiri.SetActive(false);
        GameManager.instance.gameOver.SetActive(true);
        Time.timeScale = 0;
    }
}
