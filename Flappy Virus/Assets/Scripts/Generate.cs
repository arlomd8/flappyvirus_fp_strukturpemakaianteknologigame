﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Generate : MonoBehaviour
{
    public GameObject obstacle, items;
    public int score = 0;
    public TextMeshProUGUI scoreText, scoreAkhir;
    public void Spawn()
    {
        InvokeRepeating("CreateObstacle", 1f, 1.5f);
        InvokeRepeating("CreateItem", 10f, 10f);
    }

    void CreateItem()
    {
        Instantiate(items);
    }

    private void Update()
    {
        if (GameManager.instance.gameStart)
        {
           scoreText.text = " ISOLASI DIRI HARI KE - " + score.ToString();
        }
    }
    
    void CreateObstacle()
    {
        if (GameManager.instance.gameStart)
        {
            Instantiate(obstacle);
            score++;
            GameManager.instance.scoreText.text = "ANDA TELAH MENGISOLASI DIRI SELAMA " + score + " HARI";
        }
        else
            GameManager.instance.scoreText.text = "ANDA TELAH MENGISOLASI DIRI SELAMA " + score + " HARI";

        Score.instance.collectedScore = score;

    }
}
