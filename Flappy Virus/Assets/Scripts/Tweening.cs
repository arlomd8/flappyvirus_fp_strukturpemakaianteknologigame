﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tweening : MonoBehaviour
{
    public UnityEvent onCompleteCallBack;
    public void OnEnable()
    {
        
        transform.localScale = new Vector3(0, 0, 0);
        LeanTween.scale(gameObject, new Vector3(1, 1, 1), 0.2f).setDelay(0.8f).setOnComplete(OnComplete);
        
    }
    public void OnComplete()
    {
        if (onCompleteCallBack != null)
        {
            onCompleteCallBack.Invoke();
        }
    }
}
