﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public Vector2 velocity = new Vector2(-4, 0);
    public float range = 4;
    public Sprite[] sprite;
    void Start()
    {
        int x = Random.Range(0, 7);
        this.GetComponent<SpriteRenderer>().sprite = sprite[x];
        if (GameManager.instance.gameStart)
        {
            GetComponent<Rigidbody2D>().velocity = velocity;
            transform.position = new Vector3(transform.position.x, transform.position.y - range * Random.value, transform.position.z);
        }
    }

    void Update()
    {
        if (!GameManager.instance.gameStart)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Destroyer"))
        {
            Destroy(this.gameObject);
        }

        if (collision.CompareTag("Player"))
        {
            Destroy(this.gameObject);
        }
    }
}

