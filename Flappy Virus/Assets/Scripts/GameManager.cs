﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public bool gameStart;
    public static GameManager instance;
    public GameObject gameOver, isolasiDiri, pausePanel;
    public Generate generate;
    public TextMeshProUGUI scoreText,bgmText,sfxText;
    public int score;
    void Start()
    {
        Time.timeScale = 1;
        gameStart = true;
        generate.Spawn();
        gameOver.SetActive(false);
    }
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            pausePanel.SetActive(true);
            Time.timeScale = 0;
        }
    }
    public void BGMClick()
    {
        if (Menu.bgmOn)
        {
            bgmText.text = "OFF";
            Menu.bgmOn = false;
            FindObjectOfType<AudioManager>().Mute("Theme");
        }
        else
        {
            bgmText.text = "ON";
            Menu.bgmOn = true;
            FindObjectOfType<AudioManager>().UnMute("Theme");
        }
    }
    public void SFXClick()
    {
        if (Menu.sfxOn)
        {
            sfxText.text = "OFF";
            Menu.sfxOn = false;
        }
        else
        {
            //FindObjectOfType<AudioManager>().Play("Click");
            sfxText.text = "ON";
            Menu.sfxOn = true;
        }
    }

    public void UnPauseButton()
    {
        Time.timeScale = 1;
    }

    public void HomeButton()
    {
        SceneManager.LoadScene(0);
    }


    public void ButtonClick()
    {
        if (Menu.sfxOn)
        {
            FindObjectOfType<AudioManager>().Play("Click");
        }
    }
    public void ContinueButton()
    {
        Time.timeScale = 1;
       
        SceneManager.LoadScene(3);
    }

    public void RetryButton()
    {
        Time.timeScale = 1;
        Score.instance.collectedScore = 0;
        Score.instance.highScore = 0;
        Application.LoadLevel(Application.loadedLevel);
    }
}
